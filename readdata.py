import threading
from typing import List
from locals import EMPTY_CELL_COLOR
import time

class DataReader:

    def __init__(self, file_name: str ='tempfile'):
        self.file_name: str = file_name
        self.running: bool = False
        self.frame_count: int = 0
        self.curr_piece_x: int = 0
        self.curr_piece_y: int = 0
        self.curr_piece: int = 0
        self.during_lock_delay: bool = False
        self.level: int = 0
        self.fall_timer: int = 0
        self.das: int = 0
        self.lines: int = 0
        self.score: int = 0
        self.next_piece: int = 0
        self.field: List[List[bool]] = [[False for _ in range(10)] for _ in range(20)]
        self.file_content: str = '0'
        self.thread: threading.Thread = threading.Thread(target=self.update_loop)

    def update(self):
        try:
            with open(self.file_name, 'r') as file:
                content = file.read()
            if content == self.file_content:
                return
            self.file_content = content
            self.frame_count += 1
            if content == "0":
                self.running = False
                return
            self.running = True
            content_list = content.split("\n")
            self.during_lock_delay = content_list[6] != "1"
            if not self.during_lock_delay:
                self.curr_piece_x = int(content_list[0])
                self.curr_piece_y = int(content_list[1])
                self.curr_piece = int(content_list[2])
            self.level = int(content_list[3])
            self.fall_timer = int(content_list[4])
            self.das = int(content_list[5])
            self.lines = int(content_list[7])
            self.score = int(content_list[8])
            self.next_piece = int(content_list[9])
            field_flat = content_list[10].split(" ")
            self.field = [[int(field_flat[10*i+j]) != EMPTY_CELL_COLOR for j in range(10)] for i in range(20)]
        except Exception:
            self.running = False


    def update_loop(self):
        while True:
            self.update()
            for i in self.field:
                row = '|'
                for j in i:
                    if j:
                        row += 'O'
                    else:
                        row += ' '
                row += '|'
                print(row)
            print()
            print('-'*10)
            print()

if __name__ == "__main__":
    reader = DataReader()
    reader.thread.start()