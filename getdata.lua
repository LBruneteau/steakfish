
while true do
    local in_game = (memory.readbyte(0x00c0) == 4);
    local is_paused = (bit.band(memory.readbyte(0x00b6), 16) ~= 0);
    local state = memory.readbyte(0x0048);
    tempfile = io.tempfile
    filewrite = io.open("tempfile", "w")
    if ((in_game and not is_paused and state > 0 and state < 10)) then
        local pieceX = memory.readbyte(0x0040)
        local pieceY = memory.readbyte(0x0041)
        local pieceID = (memory.readbyte(0x0042))
        local currPieceData = tostring(pieceX) .. "\n" .. tostring(pieceY) .. "\n" .. tostring(pieceID) .. "\n"
        filewrite:write(currPieceData)

        local speedLevel = memory.readbyte(0x0044)
        local fallTimer = memory.readbyte(0x0045)
        local das = memory.readbyte(0x0046)
        local currSpeeds = tostring(speedLevel) .. "\n" .. tostring(fallTimer) .. "\n" .. tostring(das) .. "\n"
        filewrite:write(currSpeeds)

        filewrite:write(tostring(memory.readbyte(0x0048)) .. "\n") -- state
        local linesStr = string.format("%x", tostring(memory.readbyte(0x0051))) .. string.format("%x", tostring(memory.readbyte(0x0050))) -- lines
        local normalLines = ""
        for j = 1, #linesStr do
            normalLines = normalLines .. tostring(tonumber(linesStr:sub(j, j), 16))
        end
        filewrite:write(normalLines .. "\n")
        local scoreStr = string.format("%x", tostring(memory.readbyte(0x0055))) .. string.format("%x", tostring(memory.readbyte(0x0054))) .. string.format("%x", tostring(memory.readbyte(0x0053)))
        local normalScore = ""
        for j = 1, #scoreStr do
            normalScore = normalScore .. tostring(tonumber(scoreStr:sub(j, j), 16))
        end
        filewrite:write(normalScore .. "\n")
        filewrite:write(memory.readbyte(0x00BF) .. "\n") -- next pieceID
        local field = ""
        for i=0x400, 0x04CF, 1 do
            field = field .. tostring(memory.readbyte(i)) .. " "
        end
        filewrite:write(field)
    else
        filewrite:write("0")
    end;
    filewrite:close()
    FCEU.frameadvance();
end;

