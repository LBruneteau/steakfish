from typing import Dict, Set, Tuple

T_UP: int = 0
T_RIGHT: int = 1
T_DOWN: int = 2
T_LEFT: int = 3
T_DEFAULT: int = T_DOWN
T_PIECE: Tuple[int, int, int, int] = (T_UP, T_RIGHT, T_DOWN, T_LEFT)
T_UP_SHAPE: Set[Tuple[int, int]] = {(-1, 0), (0, 0), (1, 0), (0, -1)}
T_RIGHT_SHAPE: Set[Tuple[int, int]] = {(0, -1), (0, 0), (1, 0), (0, 1)}
T_DOWN_SHAPE: Set[Tuple[int, int]] = {(-1, 0), (0, 0), (1, 0), (0, 1)}
T_LEFT_SHAPE: Set[Tuple[int, int]] = {(0, -1), (-1, 0), (0, 0), (0, 1)}
T_SHAPES: Tuple[Set[Tuple[int, int]], Set[Tuple[int, int]], Set[Tuple[int, int]], Set[Tuple[int, int]]] = (T_UP_SHAPE, T_RIGHT_SHAPE, T_DOWN_SHAPE, T_LEFT_SHAPE)
T_LETTER: str = 'T'

J_LEFT: int = 4
J_UP: int = 5
J_RIGHT: int = 6
J_DOWN: int = 7
J_DEFAULT: int = J_DOWN
J_PIECE: Tuple[int, int, int, int] = (J_LEFT, J_UP, J_RIGHT, J_DOWN)
J_LEFT_SHAPE: Set[Tuple[int, int]] = {(0, -1), (0, 0), (-1, 1), (0, 1)}
J_UP_SHAPE: Set[Tuple[int, int]] = {(-1, -1), (-1, 0), (0, 0), (1, 0)}
J_RIGHT_SHAPE: Set[Tuple[int, int]] = {(0, -1), (1, -1), (0, 0), (0, 1)}
J_DOWN_SHAPE: Set[Tuple[int, int]] = {(-1, 0), (0, 0), (1, 0), (1, 1)}
J_SHAPES: Tuple[Set[Tuple[int, int]], Set[Tuple[int, int]], Set[Tuple[int, int]], Set[Tuple[int, int]]] = (J_LEFT_SHAPE, J_UP_SHAPE, J_RIGHT_SHAPE, J_DOWN_SHAPE)
J_LETTER: str = 'J'

Z_HORIZONTAL: int = 8
Z_VERTICAL: int = 9
Z_DEFAULT: int = Z_HORIZONTAL
Z_PIECE: Tuple[int, int, int, int] = (Z_HORIZONTAL, Z_VERTICAL)
Z_HORIZONTAL_SHAPE: Set[Tuple[int, int]] = {(-1, 0), (0, 0), (0, 1), (1, 1)}
Z_VERTICAL_SHAPE: Set[Tuple[int, int]] = {(1, -1), (0, 0), (1, 0), (0, 1)}
Z_SHAPES: Tuple[Set[Tuple[int, int]], Set[Tuple[int, int]]] = (Z_HORIZONTAL_SHAPE, Z_VERTICAL_SHAPE)
Z_LETTER: str = 'Z'

O: int = 10
O_DEFAULT: int = O
O_PIECE: Tuple[int] = (O,)
O_SHAPE: Set[Tuple[int, int]] = {(-1, 0), (0, 0), (-1, 1), (0, 1)}
O_SHAPES: Tuple[Set[Tuple[int, int]]] = (O_SHAPE,)
O_LETTER: str = 'O'

S_HORIZONTAL: int = 11
S_VERTICAL: int = 12
S_DEFAULT: int = S_HORIZONTAL
S_PIECE: Tuple[int, int] = (S_HORIZONTAL, S_VERTICAL)
S_HORIZONTAL_SHAPE: Set[Tuple[int, int]] = {(0, 0), (1, 0), (-1, 1), (0, 1)}
S_VERTICAL_SHAPE: Set[Tuple[int, int]] = {(0, -1), (0, 0), (1, 0), (1, 1)}
S_SHAPES: Tuple[Set[Tuple[int, int]], Set[Tuple[int, int]]] = (S_HORIZONTAL_SHAPE, S_VERTICAL_SHAPE)
S_LETTER: str = 'S'

L_RIGHT: int = 13
L_DOWN: int = 14
L_LEFT: int = 15
L_UP: int = 16
L_DEFAULT: int = L_DOWN
L_PIECE: Tuple[int, int, int, int] = (L_RIGHT, L_DOWN, L_LEFT, L_UP)
L_RIGHT_SHAPE: Set[Tuple[int, int]] = {(0, -1), (0, 0), (0, 1), (1, 1)}
L_DOWN_SHAPE: Set[Tuple[int, int]] = {(-1, 0), (0, 0), (1, 0), (-1, 1)}
L_LEFT_SHAPE: Set[Tuple[int, int]] = {(-1, -1), (0, -1), (0, 0), (0, 1)}
L_UP_SHAPE: Set[Tuple[int, int]] = {(1, -1), (-1, 0), (0, 0), (1, 0)}
L_SHAPES: Tuple[Set[Tuple[int, int]], Set[Tuple[int, int]], Set[Tuple[int, int]], Set[Tuple[int, int]]] = (L_RIGHT_SHAPE, L_DOWN_SHAPE, L_LEFT_SHAPE, L_UP_SHAPE)
L_LETTER: str = 'L'

I_VERTICAL: int = 17
I_HORIZONTAL: int = 18
I_DEFAULT: int = I_HORIZONTAL
I_PIECE: Tuple[int, int] = (I_VERTICAL, I_HORIZONTAL)
I_VERTICAL_SHAPE: Set[Tuple[int, int]] = {(0, -2), (0, -1), (0, 0), (0, 1)}
I_HORIZONTAL_SHAPE: Set[Tuple[int, int]] = {(-2, 0), (-1, 0), (0, 0), (1, 0)}
I_SHAPES: Tuple[Set[Tuple[int, int]], Set[Tuple[int, int]]] = (I_VERTICAL_SHAPE, I_HORIZONTAL_SHAPE)
I_LETTER: str = 'I'

DEFAULT_TO_PIECE: Dict[int, Tuple[int, ...]] = {
    T_DEFAULT: T_PIECE,
    J_DEFAULT: J_PIECE,
    Z_DEFAULT: Z_PIECE,
    O_DEFAULT: O_PIECE,
    S_DEFAULT: S_PIECE,
    L_DEFAULT: L_PIECE,
    I_DEFAULT: I_PIECE
    }

ORIENTATION_TO_SHAPE: Dict[int, Set[Tuple[int, int]]] = {
    T_UP: T_UP_SHAPE,
    T_RIGHT: T_RIGHT_SHAPE,
    T_DOWN: T_DOWN_SHAPE,
    T_LEFT: T_LEFT_SHAPE,
    J_LEFT: J_LEFT_SHAPE,
    J_UP: J_UP_SHAPE,
    J_RIGHT: J_RIGHT_SHAPE,
    J_DOWN: J_DOWN_SHAPE,
    Z_HORIZONTAL: Z_HORIZONTAL_SHAPE,
    Z_VERTICAL: Z_VERTICAL_SHAPE,
    O: O_SHAPE,
    S_HORIZONTAL: S_HORIZONTAL_SHAPE,
    S_VERTICAL: S_VERTICAL_SHAPE,
    L_RIGHT: L_RIGHT_SHAPE,
    L_DOWN: L_DOWN_SHAPE,
    L_LEFT: L_LEFT_SHAPE,
    L_UP: L_UP_SHAPE,
    I_VERTICAL: I_VERTICAL_SHAPE,
    I_HORIZONTAL: I_HORIZONTAL_SHAPE
}

EMPTY_CELL_COLOR: int = 239
IOT_CELL_COLOR: int = 0x7B
JS_CELL_COLOR: int = 0X7C
LZ_CELL_COLOR: int = 0x7D

GRAVITY_CHART: Tuple[int, ...] = (48, 43, 38, 33, 28, 23, 18, 13, 8, 6, 5, 5, 5, 4, 4, 4, 3, 3, 3, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1)